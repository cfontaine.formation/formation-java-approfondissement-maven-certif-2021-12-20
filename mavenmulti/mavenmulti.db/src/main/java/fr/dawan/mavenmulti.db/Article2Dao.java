package fr.dawan.mavenmulti.db;


import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.mavenmulti.db.Article2;

public class Article2Dao extends GenericDao<Article2> {

    @Override
    protected void create(Connection cnx, Article2 elm) throws IOException, SQLException {
        PreparedStatement ps = cnx.prepareStatement(
                "INSERT INTO articles(description,prix,date_fabrication) VALUES(?,?,?)",
                Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, elm.getDescription());
        ps.setDouble(2, elm.getPrix());
        ps.setDate(3, Date.valueOf(elm.getDateFabrication()));
        ps.executeUpdate();

        ResultSet rsKey = ps.getGeneratedKeys();
        if (rsKey.next()) {
            long id = rsKey.getLong("id");
            elm.setId(id);
        }
    }

    @Override
    protected void update(Connection cnx, Article2 elm) throws IOException, SQLException {
        PreparedStatement ps = cnx
                .prepareStatement("UPDATE articles SET description=?,prix=?,date_fabrication=? WHERE id=?");
        ps.setString(1, elm.getDescription());
        ps.setDouble(2, elm.getPrix());
        ps.setDate(3, Date.valueOf(elm.getDateFabrication()));
        ps.setLong(4, elm.getId());
        ps.executeUpdate();
    }

    @Override
    protected void delete(Connection cnx, long id) throws IOException, SQLException {
        PreparedStatement ps = cnx.prepareStatement("DELETE FROM articles WHERE id=?");
        ps.setLong(1, id);
        ps.executeUpdate();
    }

    @Override
    protected Article2 findById(Connection cnx, long id) throws IOException, SQLException {
        Article2 a = null;
        PreparedStatement ps = cnx
                .prepareStatement("SELECT description,prix,date_fabrication FROM articles WHERE id=?");
        ps.setLong(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            a = new Article2(rs.getString("description"), rs.getDouble("prix"),
                    rs.getDate("date_fabrication").toLocalDate());
            a.setId(id);
        }
        return a;
    }

    @Override
    protected List<Article2> findAll(Connection cnx) throws IOException, SQLException {
        List<Article2> lst = new ArrayList<>();
        Statement stm = cnx.createStatement();
        ResultSet rs = stm.executeQuery("SELECT id,description,prix,date_fabrication FROM articles");
        while (rs.next()) {
            Article2 ar = new Article2(rs.getString("description"), rs.getDouble("prix"),
                    rs.getDate("date_fabrication").toLocalDate());
            ar.setId(rs.getLong("id"));
            lst.add(ar);
        }
        return lst;
    }

}
