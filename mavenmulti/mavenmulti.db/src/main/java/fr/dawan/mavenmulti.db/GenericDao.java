package fr.dawan.mavenmulti.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import fr.dawan.mavenmulti.db.DbObject;

public abstract class GenericDao<T extends DbObject> {

    private static Connection cnx;

    public void saveOrUpdate(T elm, boolean close) throws IOException, SQLException {
        if (elm.getId() == 0) {
            create(getConnection(), elm);
        } else {
            update(getConnection(), elm);
        }
        closeConnection(close);
    }

    public void delete(T elm, boolean close) throws SQLException, IOException {
        delete(elm.getId(), close);
    }

    public void delete(long id, boolean close) throws IOException, SQLException {
        delete(getConnection(), id);
        closeConnection(close);
    }

    public T findById(long id, boolean close) throws IOException, SQLException {
        T tmp = findById(getConnection(), id);
        closeConnection(close);
        return tmp;
    }

    public List<T> findAll(boolean close) throws IOException, SQLException {
        List<T> tmp= findAll(getConnection());        
        closeConnection(close);
        return tmp;
    }

    protected abstract void create(Connection cnx, T elm) throws IOException, SQLException;

    protected abstract void update(Connection cnx, T elm) throws IOException, SQLException;

    protected abstract void delete(Connection cnx, long id) throws IOException, SQLException;

    protected abstract T findById(Connection cnx, long id) throws IOException, SQLException;

    protected abstract List<T> findAll(Connection cnx) throws IOException, SQLException;

    protected static Connection getConnection() throws IOException, SQLException {
        if (cnx == null) {
            Properties conf = new Properties();
            conf.load(new FileReader("mariadb.properties"));
            cnx = DriverManager.getConnection(conf.getProperty("url"), conf.getProperty("user"),
                    conf.getProperty("password"));
        }
        return cnx;
    }

    protected static void closeConnection(boolean close) throws SQLException {
        if (close && cnx != null) {
            cnx.close();
        }
    }
}
