package fr.dawan.mavenmulti.db;

import java.time.LocalDate;

public class Article2 extends DbObject {

    private String description;

    private double prix;

    private LocalDate dateFabrication;

    public Article2() {
        super();
    }

    public Article2(String description, double prix, LocalDate dateFabrication) {
        super();
        this.description = description;
        this.prix = prix;
        this.dateFabrication = dateFabrication;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public LocalDate getDateFabrication() {
        return dateFabrication;
    }

    public void setDateFabrication(LocalDate dateFabrication) {
        this.dateFabrication = dateFabrication;
    }

    @Override
    public String toString() {
        return "Article2 [description=" + description + ", prix=" + prix + ", dateFabrication=" + dateFabrication
                + ", " + super.toString() + "]";
    }
    
    
}
