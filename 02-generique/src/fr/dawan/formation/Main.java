package fr.dawan.formation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // avant java 5
        List lst =new ArrayList();
        lst.add("azerty");
        String str=(String)lst.get(0);
        
        // après java 5
        List<String> lstGen=new ArrayList<>();
        lstGen.add("azerty");
        str=lstGen.get(0);
        
        
        // Classe Générique
        Box<String,Integer> b1=new Box<>("azerty",42);
        String sb1=b1.getV1();
        System.out.println(b1 + " " + sb1);
        
//        Box<Double,Boolean> b2=new Box(12.3,false);
//        System.out.println(b2);
        Box<Double,Long> b3= new Box<>(12.3,4L);
        System.out.println(b3);
        
        // Méthode générique
        String ma1="Bonjour";
        String mb1="Hello";
        boolean tst=TestMethodeGen.<String>egalite(ma1, mb1);
        System.out.println(tst);
        
        // Wildcard
        List<String> lstStr=new ArrayList<>();
        lstStr.add("Bonjour");
        lstStr.add("Hello");
        lstStr.add("World");
        afficher(lstStr);
       // afficherGen(lstStr);
        List<Integer> lstInt=Arrays.asList(1,3,5,1);
        afficher(lstInt);
    }

    public static void afficher(Collection c) {
        for(Object o : c) {
            System.out.println(o);
        }
    }
    
    public static void afficherGen(Collection<? extends Number> c) {
       // c.add("aze");
        for(Object o : c) {
            System.out.println(o);
        }
    }
}
