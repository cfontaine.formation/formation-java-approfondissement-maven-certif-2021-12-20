package fr.dawan.formation;

public class Main {

    public static void main(String[] args) {
        // Singleton
        Singleton s = Singleton.getInstance();
        s.setData(42);
        System.out.println(s);

        Singleton s2 = Singleton.getInstance();
        System.out.println(s2);
        System.out.println(s2.getData());
        s2.setData(5);

        System.out.println(s.getData());

        // Singleton s3=new Singleton();

        Traitement tr = new Traitement();
        tr.work();

        // Factory
        Factory fact = new Factory();
        Plat p1 = fact.getPlat("salade", 3);
        System.out.println(p1.getNomPlat());
        Plat p2 = fact.getPlat("pizza", 3);
        System.out.println(p2.getNomPlat());
    }

}
