package fr.dawan.formation;

public class Factory {

    public Plat getPlat(String nom, int nbIngredient) {
        switch(nom) {
        case "salade":
            return new Salade(nbIngredient);
        case "pizza":
            return new Pizza(nbIngredient);
        }
        return null;
    }
}
