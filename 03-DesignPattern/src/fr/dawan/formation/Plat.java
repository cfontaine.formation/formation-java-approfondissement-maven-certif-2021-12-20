package fr.dawan.formation;

public abstract class Plat {

    private int nbIngredient;
    
    public Plat(int nbIngrdient) {
        this.nbIngredient = nbIngrdient;
    }



    public abstract String getNomPlat();



    public int getNbIngredient() {
        return nbIngredient;
    }



    public void setNbIngredient(int nbIngredient) {
        this.nbIngredient = nbIngredient;
    }
    
    
    
}
