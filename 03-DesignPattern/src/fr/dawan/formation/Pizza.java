package fr.dawan.formation;

public class Pizza extends Plat {

    public Pizza(int nbIngrdient) {
        super(nbIngrdient);
    }

    @Override
    public String getNomPlat() {
        return "Pizza";
    }

}
