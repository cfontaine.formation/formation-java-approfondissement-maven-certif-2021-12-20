package fr.dawan.formation;

public class Salade extends Plat {

    public Salade(int nbIngrdient) {
        super(nbIngrdient);
    }

    @Override
    public String getNomPlat() {
        return "Salade";
    }

}
