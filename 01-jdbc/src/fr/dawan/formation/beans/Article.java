package fr.dawan.formation.beans;

import java.io.Serializable;
import java.time.LocalDate;

// JavaBean => un objet java qui respecte 4 régles (après, on peut ajouter toutes les méthodes, constructeurs que l'on veut) 
// 1. un moyen de sérialisation (généralement, implémente java.io.Serializable)
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    // 2. des attributs privés
    private long id;

    private String description;

    private double prix;

    private LocalDate dateFabrication;

    // 3. un constructeur public sans arguments
    public Article() {

    }

    public Article(String description, double prix, LocalDate dateFabrication) {
        this.description = description;
        this.prix = prix;
        this.dateFabrication = dateFabrication;
    }

    // 4. des getters et setters pour chaque attribut
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public LocalDate getDateFabrication() {
        return dateFabrication;
    }

    public void setDateFabrication(LocalDate dateFabrication) {
        this.dateFabrication = dateFabrication;
    }

    @Override
    public String toString() {
        return "Article [id=" + id + ", description=" + description + ", prix=" + prix + ", dateFabrication="
                + dateFabrication + "]";
    }

}
