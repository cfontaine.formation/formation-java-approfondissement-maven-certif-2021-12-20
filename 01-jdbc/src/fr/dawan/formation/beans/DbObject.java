package fr.dawan.formation.beans;

import java.io.Serializable;

// DbObject => Classe mère de tous les objets que l'on peut persiter en base de donnée avec le Dao générique
public abstract class DbObject implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "id=" + id;
    }

}
