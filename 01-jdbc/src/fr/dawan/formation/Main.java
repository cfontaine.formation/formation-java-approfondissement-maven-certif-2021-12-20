package fr.dawan.formation;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.formation.beans.Article;
import fr.dawan.formation.beans.Article2;
import fr.dawan.formation.dao.Article2Dao;
import fr.dawan.formation.dao.ArticleDao;

public class Main {

    public static void main(String[] args) {

// Test JDBC

//        // Ajouter un article dans la table articles       
//        Article a=new Article("Télévision 4K",700.0,LocalDate.of(2021, 3, 10));
//        testInsert(a);
//        
//        // Afficher tous les articles contenu dans la table articles
//        List<Article> l=testSelect();
//        for(Article ar: l) {
//            System.out.println(ar);
//        }
//        
//        // Supprimer un article dans la table article
//        testDelete(a);
//        
//        l=testSelect();
//        for(Article ar: l) {
//            System.out.println(ar);
//        }

        
        
// Tester une transaction

//        // true -> rollback (requête annulée)
//        testTransaction(true);
//        
//        // false -> commit (requête validé)
//        testTransaction(false);

        
        
//  Test DAO
        
//        try {
//            Article a2 = new Article("Souris Gaming", 40.0, LocalDate.of(2021, 6, 8));
//            System.out.println(a2);
//
//            ArticleDao dao = new ArticleDao();
//
//            System.out.println("_____Insert_______");
//            dao.saveOrUpdate(a2);
//            System.out.println(a2);
//
//            System.out.println("_____Update_______");
//            a2.setPrix(60.0);
//            dao.saveOrUpdate(a2);
//
//            System.out.println("_____Find by Id_______");
//            long id = a2.getId();
//            Article art1 = dao.findById(id);
//            System.out.println(art1);
//
//            System.out.println("_____delete_______");
//            dao.delete(art1);
//            List<Article> lst = dao.findAll();
//            for (Article ar : lst) {
//                System.out.println(ar);
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }

// Test DAO Générique
        
        try {
            Article2 a2 = new Article2("Souris Gaming", 40.0, LocalDate.of(2021, 6, 8));
            System.out.println(a2);

            Article2Dao dao = new Article2Dao();

            System.out.println("_____Insert_______");
            dao.saveOrUpdate(a2, false);
            System.out.println(a2);

            System.out.println("_____Update_______");
            a2.setPrix(60.0);
            dao.saveOrUpdate(a2, false);

            System.out.println("_____Find by Id_______");
            long id = a2.getId();
            Article2 art1 = dao.findById(id, false);
            System.out.println(art1);

            System.out.println("_____Delete_______");
            dao.delete(art1, false);
            
            System.out.println("_____FindAll_______");
            List<Article2> lst = dao.findAll(false);
            for (Article2 ar : lst) {
                System.out.println(ar);
            }
            
            System.out.println("_____Count_______");
            System.out.println(dao.count(false));
            
            System.out.println("_____FindGreateThanPrix_______");
            lst = dao.findGreateThanPrix(150.0, true);
            for (Article2 ar : lst) {
                System.out.println(ar);
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void testInsert(Article a) {
        Connection cnx = null;
        try {
            // Chargement du pilote de la base de de données
            Class.forName("org.mariadb.jdbc.Driver");

            // Ouverture d’une connexion à la base de donnée
            cnx = DriverManager.getConnection("jdbc:mariadb://localhost:3306/formation", "root", "dawan");

            // Statement => permet d'exécuter une requète SQL
            // Statement stm=cnx.createStatement();
            // stm.executeUpdate("INSERT INTO articles(description,prix,date_fabrication)
            // VALUES('"+a.getDescription()+"',"+a.getPrix()+",'"+a.getDateFabrication().toString()+"')");

            // PrepraredStatement => permet d'exécuter aussi une requête SQL
            // à privilégier, si la requête contient des paramètres: permet d'éviter les
            // concaténations, les types sont vérifiés ...
            PreparedStatement ps = cnx.prepareStatement(
                    "INSERT INTO articles(description,prix,date_fabrication) VALUES(?,?,?)",
                    Statement.RETURN_GENERATED_KEYS); // Statement.RETURN_GENERATED_KEYS permet de récupérer la clé
                                                      // primaire généré par la bdd

            // Les méthodes setString, setDouble ... permettent de donnée des valeurs aux
            // paramètres
            // L'indice commence à 1
            ps.setString(1, a.getDescription());
            ps.setDouble(2, a.getPrix());
            ps.setDate(3, Date.valueOf(a.getDateFabrication()));

            // Il faut utiliser la méthode executeUpdate pour les requêtes INSERT, DELETE,
            // UPDATE
            ps.executeUpdate();

            // Récupérer la valeur de la clé primaire générée par la base de donnée
            ResultSet rsKey = ps.getGeneratedKeys();
            if (rsKey.next()) {
                long id = rsKey.getLong("id");
                a.setId(id);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (cnx != null) {
                try {
                    // Fermeture de la connexion à la base de donnée
                    cnx.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static List<Article> testSelect() {
        // à partir de JDBC 4, il n'est plus necessaire de charger le driver avec
        // Class.forName
        List<Article> lst = new ArrayList<>();

        // On peut utiliser un try with ressource pour fermer automatiquement la
        // connexion à la base de donnée
        try (Connection cnx = DriverManager.getConnection("jdbc:mariadb://localhost:3306/formation", "root", "dawan")) {

            // Statement => pour exécuter une requête
            Statement stm = cnx.createStatement();

            // Il faut utiliser la méthode executeQuery pour les requêtes SELECT
            // ResultSet => pour récupèrer les résultats de la requête
            ResultSet rs = stm.executeQuery("SELECT id,description,prix,date_fabrication FROM articles");

            // La méthode next -> passe au résultat suivant (ligne), retourne vrai tant
            // qu'il y a des résultats
            // (au départ, on se trouve juste avant le premier résultat (première ligne))
            while (rs.next()) {
                // Les Méthodes getString, getDouble, ... permettent de lire les valeurs des
                // colonnes
                Article ar = new Article(rs.getString("description"), rs.getDouble("prix"),
                        rs.getDate("date_fabrication").toLocalDate());
                ar.setId(rs.getLong("id"));
                lst.add(ar);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static void testDelete(Article a) {
        try (Connection cnx = DriverManager.getConnection("jdbc:mariadb://localhost:3306/formation", "root", "dawan")) {
            PreparedStatement ps = cnx.prepareStatement("DELETE FROM articles WHERE id=?");
            ps.setLong(1, a.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void testTransaction(boolean genSqlExeception) {
        Connection cnx = null;
        try {
            cnx = DriverManager.getConnection("jdbc:mariadb://localhost:3306/formation", "root", "dawan");
            // Désactivation du mode auto-commit
            cnx.setAutoCommit(false);

            PreparedStatement ps = cnx.prepareStatement(
                    "INSERT INTO articles(description,prix,date_fabrication) VALUES(?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, "Clavier");
            ps.setDouble(2, 19.0);
            ps.setDate(3, Date.valueOf(LocalDate.of(2020, 6, 4)));
            ps.executeUpdate();
            if (genSqlExeception) {
                throw new SQLException();
            }
            ps = cnx.prepareStatement("INSERT INTO articles(description,prix,date_fabrication) VALUES(?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, "Disque SSD M2 500GO");
            ps.setDouble(2, 55.0);
            ps.setDate(3, Date.valueOf(LocalDate.of(2021, 9, 25)));
            ps.executeUpdate();

            cnx.commit(); // commit() => permet de valider les requêtes
        } catch (SQLException e) {
            try {
                cnx.rollback(); // rollback() pour annuler les requêtes
                System.err.println("Rollback");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            if (cnx != null) {
                try {
                    cnx.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
