package fr.dawan.formation.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import fr.dawan.formation.beans.Article;

public class ArticleDao {

    // Persister ou mettre à jour un objet article
    public void saveOrUpdate(Article a) throws IOException, SQLException {
        // Si un article n'est pas encore persisté => id=0
        if (a.getId() == 0) {
            create(a);
        } else {
            update(a);
        }
    }

    // Effacer un article de la table articles
    public void delete(Article a) throws SQLException, IOException {
        delete(a.getId());
    }

    // Effacer un article de la table articles à partir de son id
    public void delete(long id) throws IOException, SQLException {
        try (Connection cnx = getConnection()) {
            PreparedStatement ps = cnx.prepareStatement("DELETE FROM articles WHERE id=?");
            ps.setLong(1, id);
            ps.executeUpdate();
        }
    }

    // Récupérer un article dans la table articles à partir de son id
    public Article findById(long id) throws IOException, SQLException {
        Article a = null;
        try (Connection cnx = getConnection()) {
            PreparedStatement ps = cnx
                    .prepareStatement("SELECT description,prix,date_fabrication FROM articles WHERE id=?");
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                a = new Article(rs.getString("description"), rs.getDouble("prix"),
                        rs.getDate("date_fabrication").toLocalDate());
                a.setId(id);
            }
        }
        return a;
    }

    // Récupérer tous les article dans la table articles
    public List<Article> findAll() throws IOException, SQLException {
        List<Article> lst = new ArrayList<>();
        try (Connection cnx = getConnection()) {
            Statement stm = cnx.createStatement();
            ResultSet rs = stm.executeQuery("SELECT id,description,prix,date_fabrication FROM articles");
            while (rs.next()) {
                Article ar = new Article(rs.getString("description"), rs.getDouble("prix"),
                        rs.getDate("date_fabrication").toLocalDate());
                ar.setId(rs.getLong("id"));
                lst.add(ar);
            }
        }
        return lst;
    }

    // Ajouter un article dans la table articles
    private void create(Article a) throws IOException, SQLException {
        try (Connection cnx = getConnection()) {
            PreparedStatement ps = cnx.prepareStatement(
                    "INSERT INTO articles(description,prix,date_fabrication) VALUES(?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, a.getDescription());
            ps.setDouble(2, a.getPrix());
            ps.setDate(3, Date.valueOf(a.getDateFabrication()));
            ps.executeUpdate();

            ResultSet rsKey = ps.getGeneratedKeys();
            if (rsKey.next()) {
                long id = rsKey.getLong("id");
                a.setId(id);
            }
        }
    }

    // Mettre à jour un article dans la table articles
    private void update(Article a) throws IOException, SQLException {
        try (Connection cnx = getConnection()) {
            PreparedStatement ps = cnx
                    .prepareStatement("UPDATE articles SET description=?,prix=?,date_fabrication=? WHERE id=?");
            ps.setString(1, a.getDescription());
            ps.setDouble(2, a.getPrix());
            ps.setDate(3, Date.valueOf(a.getDateFabrication()));
            ps.setLong(4, a.getId());
            ps.executeUpdate();
        }

    }

    // Créer la connexion à la base de donnée
    public static Connection getConnection() throws IOException, SQLException {
        // Les paramètres de connexion de la base de données sont stockés dans le fichiers de propriété : mariadb.properties
        // il doit être placé à la racine du projet
        Properties conf = new Properties();
        conf.load(new FileReader("mariadb.properties"));
        Connection cnx = DriverManager.getConnection(conf.getProperty("url"), 
                                                     conf.getProperty("user"),
                                                     conf.getProperty("password"));
        return cnx;
    }

}
