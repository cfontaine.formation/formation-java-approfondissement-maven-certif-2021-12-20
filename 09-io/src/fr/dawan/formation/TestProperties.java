package fr.dawan.formation;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

public class TestProperties {

    public static void main(String[] args) {
        // écriture des propriétées

        // Properties représente un ensemble persistant de propriétés (clé=valeur)
        // uniquement des chaines de caractères
        Properties prop = new Properties();
        prop.setProperty("version", "1.0");
        prop.setProperty("user", "johndoe");
        prop.setProperty("asupprimer", "123");

        prop.remove("asupprimer");

        try {
            // On peut sauver les propriétés :
            // - dans un fichier .properties
            prop.store(new FileWriter("fichiers\\test.properties"), "test d'écriture d'un fichier proprerties");
            // - dans un fichier .xml
            prop.storeToXML(new FileOutputStream("fichiers\\test-properties.xml"), "test d'écriture d'un fichier proprerties");
        } catch (IOException e) {
            e.printStackTrace();
        }
        prop.clear(); // effacer toutes les propriétés

        // Lecture des propriétées
        Properties propR = new Properties();
        try {
            propR.load(new FileReader("fichiers\\test.properties"));
            // ou
            // propL.loadFromXML(new FileInputStream("fichiers\\test-properties.xml"));  
            
            // Récupérer la valeur à partir de la clé
            System.out.println(propR.getProperty("version"));
            System.out.println(propR.getProperty("user"));
            System.out.println(propR.getProperty("existepas"));     // si la clé n'existe pas => null
            System.out.println(propR.getProperty("existepas", "default value"));    // si la clé n'existe pas => valeur par défaut

            // Récupérer un set contenant toutes les clés
            Set<Object> keys = propR.keySet();
            for (Object k : keys) {
                System.out.println("Clé=" + k);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
