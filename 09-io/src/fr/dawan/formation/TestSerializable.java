package fr.dawan.formation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class TestSerializable {

    public static void main(String[] args) {
        Utilisateur u = new Utilisateur("John", "Doe", "jd@dawan.com");
        u.setPassword("123456");
        serialisation(u, "fichiers\\utilisateur.dat");
        Utilisateur u2 = deSerialisation("fichiers\\utilisateur.dat");
        System.out.println(u2);
    }

    // Sérialisation
    // l'objet Utilisateur va être sérialiser ainsi que tous les objets qu'il contient
    // ObjectOutputStream permet de persiter un objet ou une grappe d'objet
    public static void serialisation(Utilisateur u, String path) {
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(path))) {
            os.writeObject(u);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Désérialisation
    // ObjectInputStream permet de désérialiser
    public static Utilisateur deSerialisation(String path) {
        try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(path))) {
            Object obj = is.readObject();
            if (obj instanceof Utilisateur) {
                return (Utilisateur) obj;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
