package fr.dawan.formation;
import java.io.Serializable;

//Pour qu'une classe soit sérialisable, elle doit implémenter l'interface Serializable
//@SuppressWarnings("serial")
public class Utilisateur implements Serializable {

    // serialVersionUID est une clé de hachage SHA qui identifie de manière unique la classe.
    // Si la classe personne évolue et n'est plus compatible avec les objets précédamment persistés, on modifie la valeur du serialVersionUID
    // et lors de la désérialisation une exception sera générée pour signaler l'incompatibilité
    // java.io.InvalidClassException: fr.dawan.formation.beans.Personne; local class incompatible: stream classdesc serialVersionUID = 1, local class serialVersionUID = 2
    // si l'on fournit pas serialVersionUID le compilateur va en générer un (à éviter)
    private static final long serialVersionUID = 1L;

    private String prenom;
    
    private String nom;
    
    private String email;
    
    private transient String password;   // Si un attribut ne doit pas être sérialiser on ajout le mot clef transient
    
 //   private int age;
    
    private static int cpt; // les variable de classe ne sont pas sérialisée


    public Utilisateur() {
        super();
    }

    public Utilisateur(String prenom, String nom, String email) {
        super();
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    public int getAge() {
//        return age;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }

    @Override
    public String toString() {
        return "Utilisateur [prenom=" + prenom + ", nom=" + nom + ", email=" + email + ", password=" + password + "]";
    }
    
}
