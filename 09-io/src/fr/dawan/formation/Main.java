package fr.dawan.formation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {

    public static void main(String[] args) {
        ecrireText("fichiers\\test.txt");
        ecrireTextBuff("fichiers\\\\test2.txt");
      //   ecrireTextPrint("fichiers\\test3.txt");
        lireText("fichiers\\\\test.txt");
        lireTextBuff("fichiers\\\\test3.txt");
        copier("fichiers\\logo-dawan.png", "fichiers\\logo-copie.png");
        parcourir(new File("..\\01-jdbc"));
    }

    // Ecrire dans un fichier texte (sans try with ressource)
    public static void ecrireText(String path) {
        FileWriter fw = null;
        try {
            fw = new FileWriter(path);  // Ouverture du flux
            for (int i = 0; i < 10; i++) {
                fw.write("HelloWorld"); // écriture d'une chaine dans le fichier
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fw != null) {
                try {
                    fw.close();         // Fermeture du flux
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // Utilisation de try with ressource pour fermer automtiquement le flux vers le fichier (à partir du java 7)
    // try with ressource fonctionne avec tous les objets implémentant l'interface AutoCloseable
    public static void ecrireTextBuff(String path) {
        // On utilise un BufferedWriter, il n'a pas accès directement au fichier, il faut passer par un FileWriter
        // FileWriter: append -> true : on ajoute des données au fichier, false : le fichier est écrasé (par défaut)
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path, true))) {
            for (int i = 0; i < 10; i++) {
                bw.write("Hello World");
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
 // Ecrire dans un fichier texte avec PrintWriter
    public static void ecrireTextPrint(String path) {
        try (PrintWriter pw = new PrintWriter(new FileWriter(path))) {
            for (int i = 0; i < 10; i++) {
                pw.println(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    
    // Lecture d'un fichier texte
    public static void lireText(String path) {
        char[] tab = new char[10];
        try (FileReader fr = new FileReader(path)) {
            while (fr.read(tab) > 0) {  // lecture de 10 caractères maximum dans le fichier
                for (char c : tab) {    // lorsque l'on atteint la fin du fichier read retourne -1
                    System.out.println(c);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Lecture d'un fichier texte avec BufferedReader
    public static void lireTextBuff(String path) {
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            while (true) {
                String l = br.readLine();
                if (l == null) {
                    break;
                }
                System.out.println(l);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Exemple flux binaire: copier un fichier octet par octet
    public static void copier(String srcPath, String tgrPath) {
        try (FileInputStream fi = new FileInputStream(srcPath); FileOutputStream fo = new FileOutputStream(tgrPath)) {
            while (true) {
                int b = fi.read();
                if (b == -1) {
                    break;
                }
                fo.write(b);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    // Exemple File: Parcourir un système de fichiers
    public static void parcourir(File f) {
        if (f.exists()) {   // Test l'existence du fichier ou du dossier
            if (f.isDirectory()) {  // si c'est un dossier
                System.out.println("\nRépertoire=" + f.getName());     // affichage du nom du répertoire
                System.out.println("_____________________");
                File[] tabF = f.listFiles();    // Récupération du contenu du dossier
                for (File fi : tabF) {
                    parcourir(fi);  // Appel récursif sur chaque fichier du dossier
                }
            } else {
                System.out.println(f.getName());    // affichage du nom du fichier
            }
        } else {
            f.mkdir(); // s'il n'existe pas => création
        }
    }

}
