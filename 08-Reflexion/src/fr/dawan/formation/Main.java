package fr.dawan.formation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;

public class Main {

    public static void main(String[] args) {
        // Récupération de l'objet Class
        // 1 - avec .class
        Class<String> c1 = String.class;

        // 2 - avec getClass()
        String str = "Hello";
        Class<? extends String> c2 = str.getClass();

        // 3 - avec Class.forName
        try {
            Class<?> c3 = Class.forName("java.lang.String");
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        }

        // Nom de la classe
        System.out.println(c1.getName());

        // Nom de la classe mère
        System.out.println(c1.getSuperclass().getName());

        // Obtenir tous les attributs de la classe
        System.out.println(" \n___ Attributs ___");
        Field[] attr = c1.getDeclaredFields();
        for (Field f : attr) {
            System.out.println(f.getName());
        }

        // Obtenir toutes les méthodes de la classe
        System.out.println("\n___ Méthodes ___");
        Method[] meth = c1.getDeclaredMethods();
        for (Method m : meth) {
            // Nom de la méthode
            System.out.print(m.getName() + "\t");

            // Nom du type retour de la méthode
            System.out.print(m.getReturnType().getName() + "\t");

            // Nom de l'accessibilité de la méthode
            int modifieur = m.getModifiers();
            if (Modifier.isPublic(modifieur)) {
                System.out.println("public");
            } else if (Modifier.isPrivate(modifieur)) {
                System.out.println("private");
            } else if (Modifier.isProtected(modifieur)) {
                System.out.println("protected");
            }

            // Obtenir les paramètres de la méthode
            Parameter[] params = m.getParameters();
            for (Parameter param : params) {
                // Nom et nom du typedes paramètres de la méthode
                System.out.println("\t" + param.getName() + " " + param.getType().getName());
            }

            // Obtenir tous les constructeurs de la classe
            Constructor[] construct = c1.getConstructors();
            for (Constructor c : construct) {
                System.out.println(c);
            }

        }

        // Instanciation dynamique
        try {
            // Avec le constructeur par défaut
            String strDefault = c1.newInstance();
            System.out.println(strDefault);

            // avec le constructeur qui une chaine de caractère pour paramètre
            Constructor<String> constructeur;
            try {
                constructeur = c1.getConstructor(new Class[] { String.class });
                String strString = constructeur.newInstance(new Object[] { "azerty" });
                System.out.println(strString);

            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        // Invocation dynamique d'une méthode
        try {
            String strMeth = c1.getConstructor(new Class[] { String.class }).newInstance(new Object[] { "azerty123" });

            // Méthode sans paramètre
            Method mSp = c1.getMethod("toUpperCase", new Class[] {});
            System.out.println(mSp.invoke(strMeth, new Object[] {}));

            // Méthode avec paramètre
            Method mAp = c1.getMethod("substring", new Class[] { int.class });
            System.out.println(mAp.invoke(strMeth, new Object[] { 6 }));

            // Méthode static
            Method mSta = c1.getMethod("valueOf", new Class[] { boolean.class });
            System.out.println(mSta.invoke(null, new Object[] { false }));
        } catch (NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

    }

}
