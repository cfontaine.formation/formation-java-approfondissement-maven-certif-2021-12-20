package maven1;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeout;

import java.time.Duration;

import org.junit.jupiter.api.Test;

import fr.dawan.formation.maven1.Calcul2;

public class CalculTest2 {

    @Test 
    public void testDivision() {
        assertEquals(4, Calcul2.division(8,2));
//        try {
//            Calcul2.division(8, 0);    
//        }
//        catch (ArithmeticException e) {
//            assertTrue(true);
//        }
        assertThrows(ArithmeticException.class,()->{
            Calcul2.division(8, 0);
        });
    }
    
    @Test
    public void testTraitement() {
        assertTimeout(Duration.ofMillis(600), Calcul2::traitement);
    }
}
