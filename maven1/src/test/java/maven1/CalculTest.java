package maven1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import fr.dawan.formation.maven1.Calcul;

public class CalculTest {
    
    @BeforeAll
    public static void setup() {
        System.out.println("setup");
    }
    
    @AfterAll
    public static void tearDown() {
        System.out.println("tearDown");
    }
    
    @BeforeEach
    public void before() {
        System.out.println("beforeEach");
    }
    
    @AfterEach
    public void after() {
        System.out.println("afterEach");
    }
    
    @Test
    @DisplayName("Le test de la méthode somme")
    public void testSomme() {
        assertEquals(2, Calcul.somme(1, 1));
    }
    
    @Test
    public void testMutplication() {
        assertEquals(6, Calcul.multiplication(2, 3));
        assertEquals(0, Calcul.multiplication(0, 3));
        assertEquals(-3, Calcul.multiplication(-1, 3));
    }
    
}
