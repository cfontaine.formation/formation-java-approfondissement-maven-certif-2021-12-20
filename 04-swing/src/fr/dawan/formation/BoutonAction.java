package fr.dawan.formation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class BoutonAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()) {
        case "ok":
            System.out.println("Ok");
            break;
        case "notok":
            System.out.println("Not ok");
            break;   
        case "other":
            System.out.println("Other");
            break;   
        }
    }

}
