package fr.dawan.formation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Main {

    public static void main(String[] args) {
        // Création d'une fenêtre (conteneur)
        JFrame frame = new JFrame();
        frame.setTitle("Exemple Swing");
        frame.setSize(600, 400);
        frame.setMinimumSize(new Dimension(200, 100));

        // Création d'un bouton (composant)
        JButton bp1 = new JButton("Ok");
        bp1.setPreferredSize(new Dimension(150, 50));
        // Ajout du bouton au contentPane de la fenêtre
//        frame.getContentPane().add(bp1);

        JButton bp2 = new JButton("NotOk");
        bp2.setPreferredSize(new Dimension(150, 50));
        JButton bp3 = new JButton("Other");
        bp3.setPreferredSize(new Dimension(150, 50));
        // Ajout des boutons au contentPane de la fenêtre
//      frame.getContentPane().add(bp2);
//      frame.getContentPane().add(bp3);

        // Layout => positionnement des composants

        // Positionnement absolue
//         frame.setLayout(null);
//         bp1.setBounds(100, 100, 150, 50);
//         bp2.setBounds(10,5, 80, 50);
//         bp3.setBounds(200, 25 , 200, 30);

        // FlowLayout
//      frame.setLayout(new FlowLayout(FlowLayout.LEFT,50,100));

        // BorderLayout
//      frame.setLayout(new BorderLayout());
//      frame.getContentPane().add(bp1, BorderLayout.SOUTH);
//      frame.getContentPane().add(bp2, BorderLayout.NORTH);
//      frame.getContentPane().add(bp3, BorderLayout.WEST);

        // GridLayout
//      frame.setLayout(new GridLayout(4, 3, 20, 10));
//      for (int i = 4; i <= 12; i++) {
//        frame.getContentPane().add(new JButton(Integer.toString(i)));
//      }

        // Panneau (conteneur)
        JPanel pan = new JPanel();
        pan.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        pan.add(bp1);
        pan.add(bp2);
        pan.add(bp3);

        frame.setLayout(new BorderLayout());
        frame.getContentPane().add(pan, BorderLayout.SOUTH);

        // ActionListener
        // BoutonAction => classe qui implémente l'interface ActionListener
        BoutonAction ba = new BoutonAction();
        bp1.addActionListener(ba); 
        bp2.addActionListener(ba);
        bp2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Not Ok autre action listener");
                
            }
        });
        
        // MouseLister
//        bp2.addMouseListener(new MouseListener() {
//
//            @Override
//            public void mouseClicked(MouseEvent e) {
//                System.out.println("Bouton enfoncé");
//                
//            }
//
//            @Override
//            public void mousePressed(MouseEvent e) {
//                System.out.println("Bouton enfoncé");
//                
//            }
//
//            @Override
//            public void mouseReleased(MouseEvent e) {
//                System.out.println("Bouton relaché");
//                
//            }
//
//            @Override
//            public void mouseEntered(MouseEvent e) {
//                System.out.println(e.getX());
//                
//            }
//
//            @Override
//            public void mouseExited(MouseEvent e) {
//               System.out.println("Le curseur quitte le bouton bp2");
//                
//            }});
        
        // MouseAdapter => class abstraite qui implémente MouseListener
        // évite de redéfinir toutes les méthodes
        bp2.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                System.out.println("Mouse enter");
            }

        });
        bp2.addMouseMotionListener(new MouseAdapter() {

            @Override
            public void mouseMoved(MouseEvent e) {
                System.out.println(e.getX() + " " + e.getY());
            }
        });
        
        // Action Listener => Associer une action à un composant
        bp1.setActionCommand("ok");
        bp2.setActionCommand("notok");

        // Arrét du programme lorque l'on ferme la fenêtre
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Rendre la fenêtre visible
        frame.setVisible(true);

        // JOptionPane => boite de dialogue
        int choix=JOptionPane.showConfirmDialog(frame,"Message de la boite de dialog","Titre de la boite",JOptionPane.OK_CANCEL_OPTION,JOptionPane.ERROR_MESSAGE);
        if(choix==JOptionPane.OK_OPTION) {
            String valeur=JOptionPane.showInputDialog(frame, "Entrer une valeur",123);
            System.out.println(valeur);
        }

    }

}
