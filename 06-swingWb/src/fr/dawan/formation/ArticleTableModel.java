 package fr.dawan.formation;

import java.time.LocalDate;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import fr.dawan.formation.beans.Article2;

public class ArticleTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    private List<Article2> data;

    public ArticleTableModel(List<Article2> data) {
        super();
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Article2 row = data.get(rowIndex);
        switch (columnIndex) {
        case 0:
            return row.getId();
        case 1:
            return row.getDescription();
        case 2:
            return row.getPrix();
        case 3:
            return row.getDateFabrication();
        }
        return null;
    }

    
    
    
    @Override
    public String getColumnName(int column) {
        switch (column) {
        case 0:
            return "Id";
        case 1:
            return "Description";
        case 2:
            return "Prix";
        case 3:
            return "Date fabrication";
        }
        return"";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case 0:
            return Long.class;
        case 1:
            return String.class;
        case 2:
            return Double.class;
        default:
            return LocalDate.class;
        }

    }
    
    

}
