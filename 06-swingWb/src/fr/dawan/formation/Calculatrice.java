package fr.dawan.formation;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class Calculatrice extends JFrame {

    private static final long serialVersionUID = 1L;

    private final static String[] BTN_TEXT = { "7", "8", "9", "+", "4", "5", "6", "-", "1", "2", "3", "*", "0", "C",
            ".", "/" };

    private JPanel contentPane;

    private double v1;

    private String op;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Calculatrice frame = new Calculatrice();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public Calculatrice() {
        setTitle("Calculatrice");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JLabel lbAffichage = new JLabel("0");
        lbAffichage.setHorizontalAlignment(SwingConstants.RIGHT);
        lbAffichage.setFont(new Font("Tahoma", Font.PLAIN, 30));
        contentPane.add(lbAffichage, BorderLayout.NORTH);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(new GridLayout(4, 4, 5, 5));

        JButton[] btn = new JButton[16];
        for (int i = 0; i < 16; i++) {
            btn[i] = new JButton(BTN_TEXT[i]);
            btn[i].setActionCommand(BTN_TEXT[i]);
            panel.add(btn[i]);
            if (i != 3 && i != 7 && i != 11 && i < 13) {
                // 0 1 2 3 4 5 6 7 8 9
                btn[i].addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String strScreen;
                        if ("=".equals(op)) {
                            strScreen = "0";
                            op = null;
                        } else {
                            strScreen = lbAffichage.getText();
                        }

                        String strKey = e.getActionCommand();
                        if (strScreen.equals("0")) {
                            if (!(strScreen.length() == 1 && strKey.equals("0"))) {
                                lbAffichage.setText(strKey);
                            }
                        } else {
                            lbAffichage.setText(strScreen + strKey);
                        }
                    }
                });
            } else if (i < 13 || i == 15) {
                // opérateur: + - */
                btn[i].addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (op != null && !op.equals("=")) {
                            resultat(lbAffichage.getText());
                        } else {
                            v1 = Double.parseDouble(lbAffichage.getText());
                        }
                        lbAffichage.setText("0");
                        op = e.getActionCommand();

                    }
                });
            }

        }

        // C
        btn[13].addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                lbAffichage.setText("0");
                op = null;
            }
        });

        // .
        btn[14].addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String strScreen = lbAffichage.getText();
                if (!strScreen.contains(".")) {
                    lbAffichage.setText(strScreen + ".");
                }
            }
        });

        // =
        JButton btnNewButton = new JButton("=");
        btnNewButton.setFont(new Font("Stencil", Font.PLAIN, 20));
        btnNewButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                String tmp = resultat(lbAffichage.getText());
                lbAffichage.setText(tmp);
                op = "=";
            }

        });

        contentPane.add(btnNewButton, BorderLayout.EAST);

    }

    private String resultat(String str) {
        Double d = Double.parseDouble(str);
        switch (op) {
        case "+":
            v1 += d;
            break;
        case "-":
            v1 -= d;
            break;
        case "*":
            v1 *= d;
            break;
        case "/":
            if (v1 != 0.0) {
                v1 /= d;
            } else {
                JOptionPane.showMessageDialog(this, "Division par 0", "Erreur", JOptionPane.ERROR_MESSAGE);
            }
            break;
        }
        return Double.toString(v1);
    }

}
