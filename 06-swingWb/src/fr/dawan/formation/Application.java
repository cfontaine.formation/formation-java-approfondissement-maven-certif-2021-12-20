package fr.dawan.formation;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import fr.dawan.formation.dao.Article2Dao;


// Le projet dépend du projet 01-jbdc: pour les classes Article2 et Article2Dao
// Pour ajouter le projet en dépendance:
// Properties-> Java Build Path -> onglet Projets -> Add ... -> cocher le projet 01-jbdc

public class Application extends JFrame {

    private static final long serialVersionUID = 1L;
    
    private JPanel contentPane;
    private JTable table;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Application frame = new Application();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public Application() {
        setTitle("Article Manager");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 633, 427);
        setMinimumSize(new Dimension(500,250));
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        
        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.SOUTH);
        
        JButton btnQuitter = new JButton("Quitter");
     
        table = new JTable();
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        Article2Dao dao;
        try {
            dao = new Article2Dao();
            table.setModel(new ArticleTableModel(dao.findAll(true)));
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        // Il faut placer table dans un JScrollPane pour que le titre des colonnes s'affiche
        contentPane.add(new JScrollPane(table), BorderLayout.CENTER);
        btnQuitter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                 setVisible(false);
                 dispose();
                 System.exit(0);
            }
        });
        panel.add(btnQuitter);
    }

}
